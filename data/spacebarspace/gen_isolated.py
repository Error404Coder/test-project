from sys import *
from kg.generators import * ### @import
from formatter import * ### @import

import string

@listify
def gen_random(rand, *args):
    T, N = map(int, args[:2])
    for cas in range(T):
        n = rand.randint(1, N)
        if n==2:
            n = 3
        A = [' ']*n
        i = 0
        while i<n:
            if i==n-2:
                pass
            elif i==0 or i==n-1 or A[i-1]!=' ' and rand.random()<0.5:
                A[i] = rand.choice(string.ascii_letters)
            else:
                A[i] = ' '
            i += 1
        yield ''.join(A)


if __name__ == '__main__':
    write_to_file(print_to_file, gen_random, argv[1:], stdout)
