from itertools import islice
from sys import *
from kg.generators import * ### @import
from formatter import * ### @import

import string

@listify
def gen_full(rand, *args):
    args = iter(args)
    T, N = map(int, islice(args, 2))
    probbiglet, probbigword = map(float, islice(args, 2))

    for it in range(T):
        longword = [rand.choice(string.ascii_lowercase) for i in range(N)]
        if rand.random() < probbigword:
            sp = rand.randrange(N)
            for i in range(N):
                if i == sp or rand.random() < probbiglet:
                    longword[rand.randrange(N)] = rand.choice(string.ascii_uppercase)
        yield ''.join(longword)


if __name__ == '__main__':
    write_to_file(print_to_file, gen_full, argv[1:], stdout)
