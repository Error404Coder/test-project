from sys import *
from kg.generators import * ### @import
from formatter import * ### @import

import string

@listify
def gen_random(rand, *args):
    T, N = map(int, args[:2])

    chars = string.ascii_lowercase
    spaces = ' '*5
    for cas in range(T):
        A = [rand.choice(chars)] + rand.choices(chars+spaces,k=N-2) + [rand.choice(chars)]
    
        i = 0
        while i<N:
            j = i
            while j<N and A[j]!=' ':
                j += 1
            j -= 1

            op = rand.randrange(7)
            if op==0: # random
                k = rand.randint(i,j)
                A[k] = A[k].upper()
            elif op==1: # left is infected
                A[i] = A[i].upper()
            elif op==2: # right is infected
                A[j] = A[j].upper()
            elif op==3: # center-left is infected
                k = (i+j)//2
                A[k] = A[k].upper()
            elif op==4: # center-right is infected
                k = (i+j+1)//2
                A[k] = A[k].upper()
            elif op==5: # left is not infected
                for k in range(i+1,j+1):
                    A[k] = A[k].upper()
            elif op==6: # right is not infected
                for k in range(i,j):
                    A[k] = A[k].upper()

            j += 2
            while j<N and A[j]==' ':
                j += 1
            i = j

        yield ''.join(A)


if __name__ == '__main__':
    write_to_file(print_to_file, gen_random, argv[1:], stdout)
