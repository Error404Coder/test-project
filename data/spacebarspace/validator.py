from sys import *
from kg.validators import * ### @import

from string import ascii_letters

bounds = {
    'c': 1 <= +Var <= 300,
    'L': 0 <= +Var <= 300,
}

validchars = ascii_letters + ' '
@validator(bounds=bounds)
def validate_file(file, *, lim):

    c = file.read_int_eoln(lim.c)
    for _ in range(c):
        A = file.read_line_eoln(charset=validchars, l=lim.L)
        ensure(not A.startswith(' ') and not A.endswith(' '))
    file.read_eof()
        
    

if __name__ == '__main__':
    validate_file(stdin)
