#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    int c;
    scanf("%d\n",&c);
    while(c--){
        string line = "";
        getline(cin,line);
        string temp = "";
        bool isInfected = false;
        for(int i=0;i<line.length();i++){
            if(line[i] == ' '){
                if(isInfected){
                    for(int j=0;j<temp.length();j++){
                        putchar(toupper(temp[j]));
                    }
                }
                else{
                    cout << temp;
                }
                cout << ' ';
                temp = "";
                isInfected = false;
            }else if( isInfected || (line[i] == tolower(line[i]))){
                temp.push_back(line[i]);
            }
            else{
                temp.push_back(line[i]);
                isInfected = true;
            }
        }
        if(isInfected){
            for(int j=0;j<temp.length();j++){
                putchar(toupper(temp[j]));
            }
        }
        else{
            cout << temp;
        }
        cout << endl;
    }
}
