from sys import *
from kg.generators import * ### @import
from formatter import * ### @import

import string

@listify
def gen_random(rand, *args):
    T, N = map(int, args[:2])
    for cas in range(T):
        n = (N-1)//2
        yield ' '.join(rand.choices(string.ascii_letters,k=n+1))


if __name__ == '__main__':
    write_to_file(print_to_file, gen_random, argv[1:], stdout)
