from sys import *
from kg.generators import * ### @import
from formatter import * ### @import

import string

@listify
def gen_random(rand, *args):
    T, N = map(int, args[:2])

    chars = string.ascii_lowercase
    spaces = ' '*10
    for cas in range(T):
        n = rand.randint(0, N)
        if n==0:
            yield ""
        elif n==1:
            yield rand.choice(chars)
        else:
            yield rand.choice(chars) + ''.join(rand.choices(chars+spaces,k=n-2)) + rand.choice(chars)


if __name__ == '__main__':
    write_to_file(print_to_file, gen_random, argv[1:], stdout)
