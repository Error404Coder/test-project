# print the data into the correct input format

from kg.formatters import * ### @import

@formatter()
def print_to_file(file, cases, *, print):
    print(len(cases))
    for c in cases:
        print(c)
