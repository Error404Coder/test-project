for cas in range(int(input())):
    A = [a for a in input()]
    for i in range(1,len(A)):
        if A[i-1].isupper():
            A[i] = A[i].upper()
    for i in range(len(A)-2,-1,-1):
        if A[i+1].isupper():
            A[i] = A[i].upper()
    print(''.join(A))
